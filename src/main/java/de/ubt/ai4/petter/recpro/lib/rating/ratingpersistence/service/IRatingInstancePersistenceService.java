package de.ubt.ai4.petter.recpro.lib.rating.ratingpersistence.service;

import de.ubt.ai4.petter.recpro.lib.rating.ratingpersistence.model.RatingInstance;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface IRatingInstancePersistenceService {

    List<RatingInstance> getAll();
    List<RatingInstance> getByBpmElementId(String bpmElementId);
    void save(RatingInstance instance);

    List<RatingInstance> getByUserIds(List<String> userIds);
}
