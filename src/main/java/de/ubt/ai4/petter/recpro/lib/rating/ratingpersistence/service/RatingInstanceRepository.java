package de.ubt.ai4.petter.recpro.lib.rating.ratingpersistence.service;

import de.ubt.ai4.petter.recpro.lib.rating.ratingpersistence.model.RatingInstance;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface RatingInstanceRepository extends JpaRepository<RatingInstance, Long> {

    List<RatingInstance> getByUserIdIn(List<String> userIds);

}
