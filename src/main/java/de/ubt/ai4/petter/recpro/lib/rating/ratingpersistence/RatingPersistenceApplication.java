package de.ubt.ai4.petter.recpro.lib.rating.ratingpersistence;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RatingPersistenceApplication {

	public static void main(String[] args) {
		SpringApplication.run(RatingPersistenceApplication.class, args);
	}

}
