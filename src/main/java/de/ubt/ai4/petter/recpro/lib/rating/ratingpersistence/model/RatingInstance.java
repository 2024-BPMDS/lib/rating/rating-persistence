package de.ubt.ai4.petter.recpro.lib.rating.ratingpersistence.model;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import de.ubt.ai4.petter.recpro.lib.rating.rating.modeling.model.RatingType;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter

@Entity
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "ratingType", visible = true)
@JsonSubTypes({
        @JsonSubTypes.Type(value = BinaryRatingInstance.class, name = "BINARY"),
        @JsonSubTypes.Type(value = ContinuousRatingInstance.class, name = "CONTINUOUS"),
        @JsonSubTypes.Type(value = IntervalBasedRatingInstance.class, name = "INTERVAL_BASED"),
        @JsonSubTypes.Type(value = OrdinalRatingInstance.class, name = "ORDINAL"),
        @JsonSubTypes.Type(value = UnaryRatingInstance.class, name = "UNARY")
})

@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public class RatingInstance implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long id;
    private String recproElementId;
    private String recproElementInstanceId;
    private String ratingId;

    @Enumerated(EnumType.STRING)
    private RatingType ratingType;
    private String recproProcessInstanceId;
    private String userId;
}
